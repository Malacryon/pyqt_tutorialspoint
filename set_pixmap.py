# Copyright 2022
# Tutorialspoint
# Author: Marvin Santos

# The original sourcecode from tutorialspoint doesn't work well
# that's why i modified it a little bit

import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

def window():
    app = QApplication(sys.argv)
    win = QWidget()

    l1 = QLabel()
    l2 = QLabel()
    l3 = QLabel()
    l4 = QLabel()

    l1.setText("Hallo Welt")
    l1.setTextInteractionFlags(Qt.TextSelectableByMouse)
    l2.setText("<a href='https://pythonpyqt.com'>Welcome to Python Gui Programming</a>")
    l2.setOpenExternalLinks(True)
    l2.linkHovered.connect(hovered)
    #l3.setTextInteractionFlags(Qt.TextSelectableByMouse) # Dont get it
    l4.setText("<a href='https://tutorialspoint.com/pyqt5/index.htm'>Tutorialspoint</a>")
    l4.setOpenExternalLinks(True)
    l4.linkActivated.connect(clicked)

    l3.setPixmap(QPixmap("python.png"))

    l1.setAlignment(Qt.AlignCenter)
    l3.setAlignment(Qt.AlignCenter)
    l4.setAlignment(Qt.AlignRight)



    vbox = QVBoxLayout()
    vbox.addWidget(l1)
    vbox.addStretch()
    vbox.addWidget(l2)
    vbox.addStretch()
    vbox.addWidget(l3)
    vbox.addStretch()
    vbox.addWidget(l4)

    win.setLayout(vbox)

    win.setWindowTitle("PyQt5 QLabel Demo")
    win.show()
    sys.exit(app.exec_())

def hovered():
    print("hovering")
def clicked():
    print("clicked")

if __name__ == '__main__':
    window()
